public enum Tile {
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C");

	// field ==> has to be a constant
	private final String NAME;

	// constructor ==> has to be private
	private Tile(String name) {
		this.NAME = name;
	}

	// getter
	public String getName() {
		return this.NAME;
	}

}
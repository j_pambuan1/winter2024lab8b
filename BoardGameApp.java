import java.util.Scanner;

public class BoardGameApp {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
	Board myBoard = new Board();
	//System.out.println(myBoard.toString());
/*
//Question 2
	System.out.println("\033[0;33m~~~~~~~~~~~~~~~Testing~~~~~~~~~~~~~~~~ \033[0m");
    Tile tileVar = Tile.WALL;
    System.out.println(tileVar);

	System.out.println("\033[31m~~~~~~~~~~~toString() test~~~~~~~~~~~~ \033[0m");
//Question 5
   Board myBoard = new Board();
   System.out.println(myBoard.toString());
   	System.out.println("\033[31m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \033[0m");

//Question 7

    
    System.out.println("myBoard.placeToken(-1, 2): " + myBoard.placeToken(-1, 2)); // -->returns -2
    myBoard.setGrid(0, 4, Tile.WALL);
    myBoard.setGrid(2, 3, Tile.HIDDEN_WALL);
    System.out.println("myBoard.placeToken(0, 4): " +myBoard.placeToken(0, 4)); // -->returns -1
    System.out.println("myBoard.placeToken(2, 3): " + myBoard.placeToken(2, 3)); // --> returns 1

    
    System.out.println("\033[0;33m~~~~~~~~~~~~~~~BOARD BEFORE~~~~~~~~~~~~~~~~ \033[0m");
    System.out.println(myBoard.toString());
    System.out.println("Input a row: ");
    int userRow = scan.nextInt();
    System.out.println("Input a col: ");
    int userCol = scan.nextInt();
    System.out.println("returned : " + myBoard.placeToken(userRow, userCol));
    System.out.println("\033[0;33m~~~~~~~~~~~~~~BOARD AFTER~~~~~~~~~~ \033[0m");
    System.out.println(myBoard.toString());
    
	System.out.println("\033[0;33m~~~~~~~~~~~~~~~End of Testing~~~~~~~~~~~~~~~~ \033[0m");
    
	//printlns to skip lines on the terminal for better visual
	System.out.println();
	System.out.println();
	System.out.println();
	
*/
	// Main game Loop --8
    System.out.println("Welcome user!");
    System.out.println("\033[31m~~~~~~~~~~~~initial turns and numCastles~~~~~~~~~~~~~~~ \033[0m");
    System.out.println(myBoard);

    int numCastles = 7;
    int turns = 0;

    while (numCastles > 0 && turns <= 8) {

      System.out.println("numCastles: " + numCastles);
      System.out.println("turns: " + turns);
      System.out.println("\033[31m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \033[0m");

      System.out.println("Enter row: ");
      int row = scan.nextInt();
      System.out.println("Enter col: ");
      int col = scan.nextInt();

      int result = myBoard.placeToken(row, col);
      System.out.println("placeToken result (-2, -1, 0, 1): " + result);
      System.out.println(myBoard);

      while (result < 0) {
        System.out.println("\033[34m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \033[0m");
        System.out.println("invalid row. re-input row: ");
        row = scan.nextInt();
        System.out.println("\033[34m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \033[0m");
        System.out.println("invalid col. re-input col");
        col = scan.nextInt();
        result = myBoard.placeToken(row, col);
        System.out.println(myBoard);
      }
      if (result == 1) {
        System.out.println("There was a wall at that position.");
      } else if (result == 0) {
        System.out.println("Castle tile was succesfully placed");
        numCastles--;
      }
	  turns++;
    }
    System.out.println("Final Board: \n" + myBoard);
    if (numCastles == 0) {
      System.out.println("You won!");
    } else {
      System.out.println("You lost!");
    }
  }

}

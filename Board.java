import java.util.Random;

public class Board {
	Random rand = new Random();
	// field
	private Tile[][] grid;
	private final int SIZE = 5;

	/*	
	constructor ==> should set random 
	positions to hidden_wall and everything else to blank
	*/
	public Board(){
		this.grid = new Tile[SIZE][SIZE];
			for(int i = 0; i < this.grid.length; i++){
				int randIndex = rand.nextInt(this.grid[i].length);
					for(int j = 0; j < this.grid[i].length; j++){
						grid[i][j] = Tile.BLANK;
						if(j == randIndex){
							grid[i][j] = Tile.HIDDEN_WALL;
						}
						else{
							grid[i][j] = this.grid[i][j];
						} 
					}
			}
	}
	
	
	// overriding toString() method
	public String toString() {
		String returnedString = "";
		for (int i = 0; i < this.grid.length; i++) {
			for (int j = 0; j < this.grid[i].length; j++) {
				returnedString += "|" + this.grid[i][j].getName(); // so it only shows C, _, W,
			}
			returnedString += "|\n";
		}
		return returnedString;
	}

	// instance method
	public int placeToken(int row, int col) {
		if (row < 0 || col < 0 || row >= this.SIZE || col >= this.SIZE) {
			return -2;
		}
		if (this.grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
			return -1; // returns this value when there's already CASTLE OR WALL at that given index
									// [i][j]
		} else if (this.grid[row][col] == Tile.HIDDEN_WALL) {
			grid[row][col] = Tile.WALL;
			return 1;
		} else { // unoccupied = BLANK
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}

	// remove later (this is for testing)
	
	public void setGrid(int row, int col, Tile token) {
		grid[row][col] = token;
	}

}